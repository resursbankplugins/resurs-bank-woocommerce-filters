<?php

/*
 * Plugin Name: Resurs Bank Payment Gateway (2.2) disable internal session handler outside wp-admin.
 * Plugin URI:
 * Project URI:
 * Description: Disable the internal use of sessions in the WooCommerce Plugin, but only outside wp-admin
 * Version: 1.0
 * Author: tomas.tornevall@resurs.se
 * Author URI:
 * Text Domain:
 * Domain Path:
 */

/**
 * Disable the session_start()-handler.
 *
 * @return bool
 */
function resursDisableSessionStartNoAdmin() {
    return true;
}

add_filter('resursbank_start_session_outside_admin_only', 'resursDisableSessionStartNoAdmin');
