<?php
/*
 * Plugin Name: RBPG-2.2 - Buddypress Ruleset CustomerType Plugin
 * Plugin URI:
 * Project URI:
 * Description: Control checkout with Buddypress settings.
 * Version: 1.0
 * Author: Tomas Tornevall
 * Author URI: https://test.resurs.com/docs/display/~tomas_t
 * Text Domain:
 * Domain Path:
 */

/**
 * Requirements:
 * - Buddypress with extended profile active.
 * - Field: "Current customer type" that sets the current customer type for a logged in user (named "Customer type").
 * - Field: "Disable checkout rules" with a bunch of conditions.
 *
 * Conditions:
 *  DISABLE_CHECKOUT_NATURAL   - Disable entire checkout if customer type is NATURAL
 *  DISABLE_CHECKOUT_LEGAL     - Disable entire checkout if customer type is LEGAL
 *  DISABLE_METHODS_NATURAL    - Disable NATURAL payment methods if customer type is NATURAL
 *  DISABLE_METHODS_LEGAL      - Disable LEGAL payment methods if customer type is LEGAL
 *  DISABLE_GETADDRESS_NATURAL - Do not show getAddress fields for NATURAL
 *  DISABLE_GETADDRESS_LEGAL   - Do not show getAddress fields for LEGAL
 */

/**
 * Compare a customer or buddypress userdata and mark disabled when LEGAL is active.
 *
 * @param $enabled
 * @return bool
 * @throws Exception
 */
function resursMethodsEnabledNatural($enabled)
{
    if (is_user_logged_in() && !is_admin()) {
        if (getIsCustomer('NATURAL') && getIsCondition('DISABLE_CHECKOUT_NATURAL')) {
            $enabled = false;
        }
        if (getIsCustomer('LEGAL') && getIsCondition('DISABLE_CHECKOUT_LEGAL')) {
            $enabled = false;
        }

        // See some non logical options below, that will without mercy kill the checkout.
        /*
        if (getIsCustomer('NATURAL') && getIsCondition('DISABLE_CHECKOUT_LEGAL')) {
            $enabled = false;
        }
        if (getIsCustomer('LEGAL') && getIsCondition('DISABLE_CHECKOUT_NATURAL')) {
            $enabled = false;
        }
        */
    }

    return $enabled;
}


/**
 * Check if customer type is the requested
 *
 * @param $key
 * @param $checkoutConditions
 * @return bool
 */
function getIsCustomer($key)
{
    $return = false;
    $checkoutConditions = getResursCheckoutConditions();

    if ($checkoutConditions['customerType'] === $key) {
        $return = true;
    }

    return $return;
}

/**
 * Check if condition matches a specific checkbox.
 *
 * @param $key
 * @param $checkoutConditions
 * @return bool
 */
function getIsCondition($key)
{
    $return = false;
    $checkoutConditions = getResursCheckoutConditions();

    if ($checkoutConditions['checkoutCondition'] === $key) {
        $return = true;
    }

    return $return;
}

/**
 * @param $paymentMethods
 * @return bool
 */
function resursMethodJs($paymentMethods)
{
    if (is_user_logged_in() && !is_admin()) {
        if (getIsCustomer('NATURAL') && getIsCondition('DISABLE_METHODS_LEGAL')) {
            $paymentMethods['legal'] = [];
        }
        if (getIsCustomer('LEGAL') && getIsCondition('DISABLE_METHODS_NATURAL')) {
            $paymentMethods['natural'] = [];
        }

        // Do not expect those two options below to work properly.
        if (getIsCustomer('NATURAL') && getIsCondition('DISABLE_METHODS_NATURAL')) {
            $paymentMethods['natural'] = [];
        }
        if (getIsCustomer('LEGAL') && getIsCondition('DISABLE_METHODS_LEGAL')) {
            $paymentMethods['natural'] = [];
        }
    }

    return $paymentMethods;
}

/**
 * Collect all data and set up the rules on fly when Buddypress is loaded and ready.
 *
 * @return array
 */
function getResursCheckoutConditions()
{
    // Pick up the logged in user.
    $bpLogin = (int)bp_loggedin_user_id();

    // Current choice of customer.
    $resursChosenCustomerType = bp_get_profile_field_data(['user_id' => $bpLogin, 'field' => 'Customer type']);

    // Current choice of conditions.
    $resursHideConditions = bp_get_profile_field_data(['user_id' => $bpLogin, 'field' => 'Disable checkout rules']);

    $resursCustomerType = null;
    $resursCheckoutCondition = null;

    if (is_array($resursChosenCustomerType) && count($resursChosenCustomerType)) {
        $resursCustomerType = array_pop($resursChosenCustomerType);
    } else if (is_string($resursChosenCustomerType) && !empty($resursChosenCustomerType)) {
        $resursCustomerType = $resursChosenCustomerType;
    }

    if (is_array($resursHideConditions) && count($resursHideConditions)) {
        $resursCheckoutCondition = array_pop($resursHideConditions);
    } else if (is_string($resursHideConditions) && !empty($resursHideConditions)) {
        $resursCheckoutCondition = $resursHideConditions;
    }

    return [
        'customerType' => $resursCustomerType,
        'checkoutCondition' => $resursCheckoutCondition,
    ];
}

/**
 * getAddress field rules for different customer types.
 *
 * @param $enabled
 * @return bool
 */
function resursGetAddressEnabled($enabled)
{
    if (getIsCustomer('NATURAL') && getIsCondition('DISABLE_GETADDRESS_NATURAL')) {
        $enabled = false;
    }
    if (getIsCustomer('LEGAL') && getIsCondition('DISABLE_GETADDRESS_LEGAL')) {
        $enabled = false;
    }

    return $enabled;
}

/**
 * Buddypress emulation. Requires buddypress enabled. Collects logged in user field "Customer type".
 */
function resursBuddyPressDataNatural()
{
    add_filter('resurs_bank_checkout_methods_enabled', 'resursMethodsEnabledNatural');
    add_filter('resurs_bank_js_payment_methods', 'resursMethodJs');
    add_filter('resurs_getaddress_enabled', 'resursGetAddressEnabled');
}

function initLoggedInNatural()
{
    // If BuddyPress is present, try to fetch internal data before continuing.
    if (function_exists('buddypress') /*&& class_exists('BP_Groups_Group', false)*/) {
        add_action('bp_ready', 'resursBuddyPressDataNatural', 1);
    } else {
        // If buddypress is present, start collecting info first.
        add_filter('resurs_bank_checkout_methods_enabled', 'resursMethodsEnabledNatural');
    }
}

// Make sure plugins are loaded before hunting LEGAL in profile.
add_action('plugins_loaded', 'initLoggedInNatural');
