<?php

/**
 * Plugin Name: Shipping address converter.
 * Description: If shipping address for some reason are not present during a payment process, but required from a plugin even though the address is empty - automatically fill it in with help from billing.
 * WC Tested up to: 7.8.0
 * WC requires at least: 7.6.0
 * Requires PHP: 8.1
 * Version: 1.0.0
 * Author: Resurs Bank AB
 */

if (!defined(constant_name: 'ABSPATH')) {
    exit;
}

// Whatever you do, do it in the plugins_loaded filter. Also, always add the featuresUtil
// for newer scripts as we need to bring HPOS with the snippets, to not break stuff.
add_action(hook_name: 'plugins_loaded', callback: static function (): void {
    // Always add this. ALWAYS.
    /** @noinspection PhpArgumentWithoutNamedIdentifierInspection */
    add_action(
        'before_woocommerce_init',
        function () {
            if (class_exists(\Automattic\WooCommerce\Utilities\FeaturesUtil::class)) {
                \Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility(
                    'custom_order_tables',
                    __FILE__,
                    true
                );
            }
        }
    );

    /** @noinspection PhpArgumentWithoutNamedIdentifierInspection */
    add_filter('woocommerce_get_order_address', function ($address = '', $type = '', $that = null) {
        // Check if the class exists and other conditions are met

        if ($type === 'shipping' &&
            $that instanceof WC_Order &&
            class_exists(class: 'Resursbank\Ecom\Lib\Validation\StringValidation')
        ) {
            try {
                // Validate payment method. If not uuid, consider this "not ours" and leave it.
                // This validation throws an exception if not properly validated.
                $stringValidation = (new Resursbank\Ecom\Lib\Validation\StringValidation());
                $stringValidation->isUuid(value: $that->get_payment_method());

                // Set correct billing fields
                $set_billing = [
                    'first_name' => $that->get_billing_first_name(),
                    'last_name' => $that->get_billing_last_name(),
                    'company' => $that->get_billing_company(),
                    'address_1' => $that->get_billing_address_1(),
                    'address_2' => $that->get_billing_address_2(),
                    'city' => $that->get_billing_city(),
                    'postcode' => $that->get_billing_postcode(),
                    'country' => $that->get_billing_country(),
                    'state' => $that->get_billing_state(),
                    'phone' => $that->get_billing_phone(),
                ];

                // Prepare for a new address by first inheriting the old address
                $new_address = $address;

                // Replace empty shipping fields with billing data by looping through the old address
                foreach ($address as $key => $value) {
                    // If value is empty but exists in billing, use that data instead.
                    if (empty($value) && isset($set_billing[$key]) && !empty($set_billing[$key])) {
                        $new_address[$key] = $set_billing[$key];
                    }
                }

                // Return updated shipping address
                return $new_address;
            } catch (Throwable) {
                // Return the original address if an error occurs
                return $address;
            }
        }

        // Return the original address if conditions are not met
        return $address;
    }, 10, 3);
});
