<?php

/*
 * Plugin Name: Resurs Bank Payment Gateway (2.2) disable internal session handler.
 * Plugin URI:
 * Project URI:
 * Description: Disable the internal use of sessions in the WooCommerce Plugin
 * Version: 1.0
 * Author: tomas.tornevall@resurs.se
 * Author URI:
 * Text Domain:
 * Domain Path:
 */

/**
 * Disable the session_start()-handler.
 *
 * @return bool
 */
function resursDisableSessionStart() {
    return true;
}

add_filter('resursbank_start_session_before', 'resursDisableSessionStart');
