<?php

/*
 * Plugin Name: Resurs Bank Payment Gateway (2.2) - Disable specific customer type method
 * Plugin URI:
 * Project URI:
 * Description: Disable/enable payment methods depending on customer type.
 * Version: 1.0
 * Author: tomas.tornevall@resurs.se
 * Author URI:
 * Text Domain:
 * Domain Path:
 */

function resursDetailMethodLegalDisable($enabled, $that)
{
    if (in_array('LEGAL', $that->customerTypes)) {
        $enabled = false;
    }

    return $enabled;
}

add_filter('resurs_method_is_enabled', 'resursDetailMethodLegalDisable', 10, 2);
