=== RB-Use-Sku ===
Contributors: RB-Tornevall, Tornevall
Tags: WooCommerce, Resurs Bank, Payment, Payment gateway, ResursBank, payments
Requires at least: 3.0.1
Tested up to: 5.5.3
Requires PHP: 5.4
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Force SKU instead of post id on articles in resurs woocommerce plugin.

== Description ==

= About =

Force SKU instead of post id on articles in resurs woocommerce plugin.
