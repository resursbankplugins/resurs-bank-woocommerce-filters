<?php

/*
 * Plugin Name: Resurs Bank Payment Gateway (2.2) init functions enabled
 * Plugin URI:
 * Project URI:
 * Description: Disable/enable initfunctions of Resurs Bank gateway
 * Version: 1.0
 * Author: tomas.tornevall@resurs.se
 * Author URI:
 * Text Domain:
 * Domain Path:
 */

/**
 * Disable the session_start()-handler.
 *
 * @return bool
 */
function resursInitEnabled($enabled) {
    $enabled = false;
    return $enabled;
}

add_filter('resurs_bank_init_enabled', 'resursInitEnabled');
