<?php
/**
 * Plugin Name: RB-Use-Sku
 * Description: Force SKU instead of post id on articles in resurs woocommerce plugin (for the v2.2 series).
 * Version: 1.0.0
 * Author: Resurs Bank AB
 * Author URI: https://test.resurs.com/docs/display/ecom/WooCommerce
 * Text Domain: resurs-bank-payment-gateway-for-woocommerce
 * Domain Path: /language
 */
/**
 * @param $returnValue
 * @param string $requestKey
 * @return bool|mixed
 */
function use_rb_sku($returnValue, $requestKey = '')
{
    if ($requestKey === 'useSku') {
        $returnValue = true;
    }
    return $returnValue;
}
add_filter('resurs_option', 'use_rb_sku', 10, 2);
